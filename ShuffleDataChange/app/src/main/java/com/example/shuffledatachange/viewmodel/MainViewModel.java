package com.example.shuffledatachange.viewmodel;

import android.content.Intent;
import android.view.View;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.shuffledatachange.model.NumberModel;
import com.example.shuffledatachange.view.MainActivity;
import com.example.shuffledatachange.view.StringNumberSplitActivity;
import java.util.ArrayList;
import java.util.Random;
public class MainViewModel extends ViewModel {
    private MutableLiveData<ArrayList<NumberModel>> mNumbersList = new MutableLiveData<>();
    public MainViewModel() {
        ArrayList<NumberModel> numberModels = new ArrayList<>();
        numberModels.add(new NumberModel(1, "unsatisfied"));
        numberModels.add(new NumberModel(4, "unsatisfied"));
        numberModels.add(new NumberModel(2, "unsatisfied"));
        numberModels.add(new NumberModel(3, "unsatisfied"));
        numberModels.add(new NumberModel(6, "unsatisfied"));
        numberModels.add(new NumberModel(5, "unsatisfied"));
        mNumbersList.setValue(numberModels);
    }

    public MutableLiveData<ArrayList<NumberModel>> getmNumbersList() {
        return mNumbersList;
    }

    public void onShuffleClick() {
        ArrayList<NumberModel> numberModels = randomize(mNumbersList.getValue());
        for (int i = 0; i < numberModels.size(); i++) {
            try {
                if (((i-1)!=-1&&numberModels.get(i - 1) != null && numberModels.get(i).getMnumber() - 1 == numberModels.get(i - 1).getMnumber()) && ((i+1)<numberModels.size()&&numberModels.get(i + 1) != null && numberModels.get(i).getMnumber() + 1 == numberModels.get(i + 1).getMnumber())) {
                    numberModels.get(i).setMstatus("satisfied");
                } else if (((i-1)!=-1&&numberModels.get(i - 1) != null && numberModels.get(i).getMnumber() - 1 == numberModels.get(i - 1).getMnumber()) || ((i+1)<numberModels.size()&&numberModels.get(i + 1) != null && numberModels.get(i).getMnumber() + 1 == numberModels.get(i + 1).getMnumber())) {
                    numberModels.get(i).setMstatus("half");
                } else {
                    numberModels.get(i).setMstatus("unsatisfied");
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        mNumbersList.setValue(numberModels);
    }

    public ArrayList<NumberModel> randomize(ArrayList<NumberModel> numbers) {
        Random r = new Random();
        for (int i = numbers.size() - 1; i > 0; i--) {
            int j = r.nextInt(i);
            NumberModel temp = numbers.get(i);
            numbers.set(i, numbers.get(j));
            numbers.set(j, temp);

        }

        for (int i = 0; i < numbers.size(); i++) {
            numbers.get(i).setMstatus("unsatisfied");
        }
        return numbers;
    }

    public void onNumberSplit(View view) {
        Intent i = new Intent(view.getContext(), StringNumberSplitActivity.class);
        view.getContext().startActivity(i);
    }

    public void onShuffle(View view) {
        Intent i = new Intent(view.getContext(), MainActivity.class);
        view.getContext().startActivity(i);
    }
}
