package com.example.shuffledatachange.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import com.example.shuffledatachange.R;
import com.example.shuffledatachange.adapter.BlogAdapter;
import com.example.shuffledatachange.databinding.ActivityMainBinding;
import com.example.shuffledatachange.model.NumberModel;
import com.example.shuffledatachange.viewmodel.MainViewModel;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        MainViewModel loginViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        binding.setPostdata(loginViewModel);
        binding.setLifecycleOwner(this);
        loginViewModel.getmNumbersList().observe(this, new Observer<ArrayList<NumberModel>>() {
            @Override
            public void onChanged(ArrayList<NumberModel> numberModels) {
                BlogAdapter adapter = new BlogAdapter(numberModels);
                updateAdapter(adapter);
            }
        });
    }

    private void updateAdapter(BlogAdapter adapter) {
        RecyclerView recyclerView = findViewById(R.id.blogRecyclerView);
        recyclerView.setAdapter(adapter);
    }
}
