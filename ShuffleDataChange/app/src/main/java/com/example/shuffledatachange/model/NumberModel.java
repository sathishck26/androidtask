package com.example.shuffledatachange.model;

public class NumberModel {
    private Integer mnumber;
    private String mstatus;

    public NumberModel(Integer mnumber, String mstatus) {
        this.mnumber = mnumber;
        this.mstatus = mstatus;
    }

    public Integer getMnumber() {
        return mnumber;
    }

    public void setMnumber(Integer mnumber) {
        this.mnumber = mnumber;
    }

    public String getMstatus() {
        return mstatus;
    }

    public void setMstatus(String mstatus) {
        this.mstatus = mstatus;
    }
}
