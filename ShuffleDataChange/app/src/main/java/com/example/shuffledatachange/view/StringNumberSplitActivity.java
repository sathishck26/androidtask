package com.example.shuffledatachange.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.shuffledatachange.R;
import com.example.shuffledatachange.databinding.ActivityStringNumberSplitBinding;
import com.example.shuffledatachange.viewmodel.MainViewModel;

public class StringNumberSplitActivity extends AppCompatActivity {
    private EditText medit_input,medit_output;
    Button btn_display;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityStringNumberSplitBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_string_number_split);
        MainViewModel loginViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        binding.setPostdata(loginViewModel);
        binding.setLifecycleOwner(this);
        getViewCasting();
        btn_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String  str=medit_input.getText().toString();

                if(str.equals("")){
                    Toast.makeText(getApplicationContext(),"Input is Empty",Toast.LENGTH_SHORT).show();
                }
                String numberOnly= str.replaceAll("[^0-9]", "");
                medit_output.setText(numberOnly);
            }
        });
    }
    public void getViewCasting(){
        medit_input = (EditText)findViewById(R.id.edit_input);
        medit_output = (EditText)findViewById(R.id.edit_output);
        btn_display = (Button)findViewById(R.id.btn_display);
    }
}
