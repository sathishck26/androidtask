package com.example.shuffledatachange.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import com.example.shuffledatachange.R;
import com.example.shuffledatachange.databinding.ActivityDashBoardBinding;
import com.example.shuffledatachange.databinding.ActivityMainBinding;
import com.example.shuffledatachange.viewmodel.MainViewModel;

public class DashBoardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityDashBoardBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_dash_board);
        MainViewModel loginViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        binding.setPostdata(loginViewModel);
        binding.setLifecycleOwner(this);
    }
}
