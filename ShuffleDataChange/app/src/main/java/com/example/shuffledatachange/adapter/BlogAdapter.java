package com.example.shuffledatachange.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.recyclerview.widget.RecyclerView;
import com.example.shuffledatachange.R;
import com.example.shuffledatachange.model.NumberModel;
import java.util.List;

public class BlogAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private static final String TAG = "BlogAdapter";

    private List<NumberModel> mBlogList;

    public BlogAdapter(List<NumberModel> blogList) {
        mBlogList = blogList;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.blog_item, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        if (mBlogList != null && mBlogList.size() > 0) {
            return mBlogList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends BaseViewHolder {

        Button btn;

        public ViewHolder(View itemView) {
            super(itemView);
            btn = itemView.findViewById(R.id.btn);
        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            final NumberModel mBlog = mBlogList.get(position);

            btn.setText(String.valueOf(mBlog.getMnumber()));
            if (mBlog.getMstatus().equalsIgnoreCase("satisfied")) {
                btn.setBackgroundColor(Color.GREEN);
            } else if (mBlog.getMstatus().equalsIgnoreCase("half")) {
                btn.setBackgroundColor(Color.YELLOW);
            } else {
                btn.setBackgroundColor(Color.RED);
            }

        }
    }

}
